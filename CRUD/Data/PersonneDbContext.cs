using System;
using CRUD.Model;
using Microsoft.EntityFrameworkCore;

namespace CRUD.Data
{
    public class PersonneDbContext : DbContext
    {
        public DbSet<Personne> Personnes { get; set; }

        public string DbPath { get; private set; }

        public PersonneDbContext()
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = $"{path}{System.IO.Path.DirectorySeparatorChar}personnes.db";
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
           => options.UseSqlite($"Data Source={DbPath}");
 

    }
}