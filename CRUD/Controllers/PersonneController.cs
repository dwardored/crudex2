﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using CRUD.Interface;
using CRUD.Model;
using CRUD.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace CRUD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonneController : ControllerBase
    {
        private readonly RepositoryPersonne _personneRepo;
        private readonly IRepository<Personne> _Personne;

        public PersonneController(IRepository<Personne> Personne, RepositoryPersonne personneRepo)
        {
            _personneRepo = personneRepo;
            _Personne = Personne;
        }
        //Add Person  
        [HttpPost("/Personne")]
        public async Task<Object> AddPerson([FromBody] Personne personne)
        {
            try
            {
                await _personneRepo.Create(personne);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        //Get Person  
        [HttpGet("/Personne/{Id}")]
        public Object GetPersonne(Guid Id)
        {
            try
            {
                Personne personne = _personneRepo.GetById(Id);
                return personne;

            }
            catch (Exception)
            {

                return false;
            }
        }
        //Delete personne  
        [HttpDelete("/Personne/{Id}")]
        public bool DeletePerson(Guid Id)
        {
            try
            {
                _personneRepo.Delete(Id);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Update personne
        [HttpPut("/Personne")]
        public bool UpdatePerson(Personne Object)
        {
            try
            {
                _personneRepo.Update(Object);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //GET All personne  
        [HttpGet("/Personnes")]
        public Object GetAllPersons(string nom, string prenom)
        {
            IEnumerable<Personne> data;
            if (nom == null && prenom == null)
            {
                data = _personneRepo.GetAll();
            }
            else
            {
                data = _personneRepo.Search(nom, prenom);
            }
            return data;
        }
    }

}
