﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CRUD.Data;
using CRUD.Interface;
using CRUD.Model;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq;

namespace CRUD.Repository
{
    public class RepositoryPersonne : IRepository<Personne>
    {
        PersonneDbContext _dbContext;
        public RepositoryPersonne(PersonneDbContext personneDbContext)
        {
            _dbContext = personneDbContext;
        }
        public async Task<Personne> Create(Personne _object)
        {
            EntityEntry<Personne> entityEntry = await _dbContext.Personnes.AddAsync(_object);
            _dbContext.SaveChanges();
            return entityEntry.Entity;
        }

        public void Delete(Guid id)
        {
            Personne personne = _dbContext.Personnes.Find(id);
            _dbContext.Remove(personne);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Personne> GetAll()
        {
            try
            {
                return _dbContext.Personnes;
            }
            catch (Exception ee)
            {
                throw;
            }
        }
        public IEnumerable<Personne> Search(string nom, string prenom)
        {
            try
            {
                return _dbContext.Personnes.Where(i =>
                (nom == null || (i.Nom.ToLower().StartsWith(nom.ToLower()) || i.Nom.ToLower().EndsWith(nom.ToLower())))
               && (prenom == null || (i.Prenom.ToLower().StartsWith(prenom.ToLower()) || i.Prenom.ToLower().EndsWith(prenom.ToLower())))).ToList();
            }
            catch (Exception ee)
            {
                throw;
            }
        }

        public Personne GetById(Guid Id)
        {
            return _dbContext.Personnes.Find(Id);
        }

        public void Update(Personne _object)
        {
            Personne personne = _dbContext.Personnes.Find(_object.Id);
            if (personne == null)
            {
                return;
            }
            _dbContext.Entry(personne).CurrentValues.SetValues(_object);
            _dbContext.SaveChanges();
        }
    }
}
